<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>register</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <p>First name:</p>
        <input type="text" name="fname">
        <p>Last name:</p>
        <input type="text" name="lname">
        <p>Gender:</p>
        <input type="radio" name="gender" value="Male">
        <label>Male</label><br>
        <input type="radio" name="gender" value="Female">
        <label>Female</label><br>
        <input type="radio" name="gender" value="Other">
        <label>Other</label><br>
        <p>Nationality:</p>
        <select name="nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Singaporean">Singaporean</option>
        </select><br>
        <p>Language Spoken:</p>
        <input type="checkbox" name="language" value="Indonesia">
        <label>Bahasa Indonesia</label><br>
        <input type="checkbox" name="language" English">
        <label>English</label><br>
        <input type="checkbox" name="language" value="Other">
        <label>Other</label><br>
        <p>Bio:</p>
        <textarea name="bio" cols="30" rows="10"></textarea>
        <br><input type="submit" name="submit" value="Sign Up">
    </form>

</body>

</html>